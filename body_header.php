<html lang="en">
	<head>
    <meta charset="utf-8">
    <title>HDBFS Quick Loan Application</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDBFS Quick Loan Application">
    <meta name="keywords" content="loan, application form, quick application">
    <meta name="author" content="Jaikiran Parekh">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/stylesheet.css">
	<link rel="stylesheet" href="css/select2.min.css">
	<link rel="stylesheet" href="css/datepicker.min.css">
	<link rel="stylesheet" href="css/jquery-ui.min.css"/> 

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/jquery-migrate-3.0.0.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/jquery.md5.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/select2.min.js"></script>
	<script src="js/datepicker.min.js"></script>
	<script src="js/jquery-ui.min.js"></script>    

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-101801436-3', 'auto');
  ga('send', 'pageview');
</script>
  </head>
<body>



<div style="text-align:center; position:fixed; z-index:1000; background:#004a8f; top:0; width:100%;"><img style="padding: 15px 0;" src="images/logo.jpg" alt="logo"></div>
    <div class="container">
      <div class="row clearfix">
	          <!-- Components -->
        <div class="col-md-12">
          <div class="tabbable" style="    margin-top: 90px;">
<H3 style="text-align:center;">Quick Loan Application Form</H3>
<hr>
</div>
