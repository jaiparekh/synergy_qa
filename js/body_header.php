<html lang="en">
	<head>
    <meta charset="utf-8">
    <title>HDB SIM App</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="HDBFS Sales Information Management">
    <meta name="author" content="Jaikiran Parekh">
	<meta name="robots" content="noindex">
	<link rel="icon" href="favicon.ico" type="image/x-icon">
	
	<link rel="stylesheet" href="css/bootstrap.min.css">
	<link rel="stylesheet" href="css/bootstrap-theme.min.css">
	<link rel="stylesheet" href="css/stylesheet.css">

	<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/jquery.validate.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/select2.min.js"></script>


<script>
	$(document).ready(function() {
	    $('.toggler').on('click', function(event) {
	        event.preventDefault();
	        $('.cd-panel').addClass('is-visible');
	    });
	    $('.cd-panel').on('click', function(event) {
	        if ($(event.target).is('.cd-panel') || $(event.target).is('.cd-panel-close')) {
	            $('.cd-panel').removeClass('is-visible');
	            event.preventDefault();
	        }
	    });
	    $('.cd-panel').on('click', function(event) {
	        if ($(event.target).is('.cd-panel') || $(event.target).is('.cd-panel-close img')) {
	            $('.cd-panel').removeClass('is-visible');
	            event.preventDefault();
	        }
	    });
	}); // end document.ready
</script>

    <!--[if lt IE 9]>
    <script src="//html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->
  </head>
<body>



<div style="text-align:center; position:fixed; z-index:1000; background:#004a8f; top:0; width:100%;"><img style="padding: 15px 0;" src="images/logo.jpg" alt="logo"></div>
    <div class="container">
      <div class="row clearfix">
	          <!-- Components -->
        <div class="col-md-12">
          <div class="tabbable" style="    margin-top: 90px;">
<H3 style="text-align:center;">Referral Program</H3>
<hr>
</div>