<?php
require '../phpmailer/PHPMailerAutoload.php';

//editable
$mysqli = mysqli_connect('localhost','hdb_synergy','h91$yn@rG^','hdb_synergy');
$yesterday = date("Y-m-d",strtotime("-1 days"));
$query = "SELECT quickapply.*, branch_pincode.branch_id FROM quickapply LEFT JOIN branch_pincode ON quickapply.pincode=branch_pincode.pincode WHERE quickapply.timestamp LIKE '%$yesterday%'";
$filename = date("d-m-Y_H-i-s")."_HDB_Quick_Apply_All_MIS.csv";
$email_to = "nitesh.patil@hdbfs.com";

//do not edit beyond this
$result = $mysqli->query($query);
if (!$result) die(mysqli_error($mysqli));
$num_results = mysqli_num_rows($result);
$num_fields = mysqli_num_fields($result);
$headers = array();
for ($i = 0; $i < $num_fields; $i++) {
    $headers[] = mysqli_fetch_field_direct($result , $i)->name;
}
$fp = fopen($filename, 'w') or die("could not write");
if ($fp && $result) {
    fputcsv($fp, $headers);
    while ($row = $result->fetch_array(MYSQLI_NUM)) {
        fputcsv($fp, array_values($row));
		$sendemail = "yes";
    }
	fclose($fp);
	
	
//pivot table
	
$all_property = array();

$Source_Extended = mysqli_query($mysqli, 'create OR REPLACE view `Source_Extended` as (
  select
    quickapply.source,
    case when `final_result` = "Red" then `final_result` end as `Red`,
    case when `final_result` = "Amber" then `final_result` end as `Amber`,
    case when `final_result` = "Green" then `final_result` end as `Green`
  from `quickapply` WHERE quickapply.timestamp LIKE "%'.$yesterday.'%"
)');

$Source_Extended_Pivot = mysqli_query($mysqli, 'create OR REPLACE view `Source_Extended_Pivot` as (
  select
    `source`,
    count(`Red`) as `Red`,
    count(`Amber`) as `Amber`,
    count(`Green`) as `Green`,
	count(`Red`)+count(`Amber`)+count(`Green`) as `Total`
  from `Source_Extended`
  group by `source`
)');

$Source_Extended_Pivot_Pretty_result = mysqli_query($mysqli, 'Select * FROM `Source_Extended_Pivot`');
	
$totals_table = mysqli_query($mysqli, "Select  sum(`Red`) as `red`, sum(`Amber`) as `amber` , sum(`Green`) as `green`, (sum(`Red`) + sum(`Amber`) + sum(`Green`)) as Total from Source_Extended_Pivot");
$totals_table_row = mysqli_fetch_array($totals_table);
	
//showing property
$table_html = '<table class="data-table" width="800" border="0" style="background:#fff" cell-padding="5" cell-spacing="5">
        <tr class="data-heading">';
while ($property = mysqli_fetch_field($Source_Extended_Pivot_Pretty_result)) {
    $table_html .=  '<td style="background:#87CEEB;text-align: center;" width="200"><strong>' . $property->name . '</strong></td>';
    array_push($all_property, $property->name);
}
$table_html .=  '</tr>';

//showing all data
while ($row = mysqli_fetch_array($Source_Extended_Pivot_Pretty_result)) {
    $table_html .=  "<tr>";
    foreach ($all_property as $item) {
		if($item == "Total")
		{
		$table_html .=  '<td style="background:#87CEEB;text-align: center;"><strong>' . $row[$item] . '</strong></td>';
		}else{
        $table_html .=  '<td style="background:#BFEFFF;text-align: center;">' . $row[$item] . '</td>';
		}
    }
    $table_html .=  '</tr>';
}

$table_html .= "<tr>";
$table_html .= '<td style="background:#87CEEB;text-align: center;"><strong>Total</strong></td>';
$table_html .= '<td style="background:#87CEEB;text-align: center;"><strong>' . $totals_table_row['red'] . '</strong></td>';
$table_html .= '<td style="background:#87CEEB;text-align: center;"><strong>' . $totals_table_row['amber'] . '</strong></td>';
$table_html .= '<td style="background:#87CEEB;text-align: center;"><strong>' . $totals_table_row['green'] . '</strong></td>';
$table_html .= '<td style="background:#87CEEB;text-align: center;"><strong>' . $totals_table_row['Total'] . '</strong></td>';
$table_html .= '</tr>';


$table_html .=  "</table>";

	//end pivot table
	

	if ($sendemail == "yes")
{
	
	echo "File wrote successfully <br><br>"; // <a href='".$filename."'>" . $filename . "</a>";
		
	$email = new PHPMailer();
	$email->isSMTP();
	$email->SMTPDebug = 0;
	$email->SMTPOptions = array( 'ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));
	$email->Debugoutput = 'html';
	$email->Host = "webmail.hdbfs.com";
	$email->Port = 465;
	$email->SMTPSecure = 'ssl';
	$email->SMTPAuth = true;
	$email->Username = "greenalerts";
	$email->Password = "Password@123";
	$email->setFrom('greenalerts@hdbfs.com', 'QuickApply BOT');
	$email->isHTML(true);
	$email->Subject   = 'QuickApply - MIS';
	$email->Body      = "Number of Leads: ".$num_results."<br><br> Campaign-wise break-up<br><br>".$table_html."<br><br>PFA<br><br>This is an auto-generated mail.";
	$email->AddAddress( $email_to );
	$email->AddAddress( 'jai.parekh@hdbfs.com' );
	$email->AddAddress( 'marketing.support@hdbfs.com' );
	$email->AddAddress( 'kedar.parasnis@hdbfs.com' );
	$email->AddAddress( 'sudipta.mandal@hdbfs.com' );
	//$email->AddAddress( 'venkata.swamy@hdbfs.com' );
	//$email->AddAddress( 'sanjay.belsare@hdbfs.com' );
	$email->AddAttachment( $filename , $filename );
		
		if (!$email->send()) {
			echo "Mailer Error: " . $email->ErrorInfo;
		} else {
			
			echo "Message sent! <br><br>";
			
			if (unlink($filename))
			{
			echo "File Deleted!";
			}
		}
	
	
	//send MIS
	$mis_email = new PHPMailer();
	$mis_email->isSMTP();
	$mis_email->SMTPDebug = 0;
	$mis_email->SMTPOptions = array( 'ssl' => array('verify_peer' => false, 'verify_peer_name' => false, 'allow_self_signed' => true));
	$mis_email->Debugoutput = 'html';
	$mis_email->Host = "webmail.hdbfs.com";
	$mis_email->Port = 465;
	$mis_email->SMTPSecure = 'ssl';
	$mis_email->SMTPAuth = true;
	$mis_email->Username = "greenalerts";
	$mis_email->Password = "Password@123";
	$mis_email->setFrom('greenalerts@hdbfs.com', 'QuickApply BOT');
	$mis_email->isHTML(true);
	$mis_email->Subject   = 'QuickApply - MIS';
	$mis_email->Body      = "Number of Leads: ".$num_results."<br><br> Campaign-wise break-up<br><br>".$table_html."<br><br>PFA<br><br>This is an auto-generated mail.";
	//$mis_email->AddAddress( $email_to );
	//$mis_email->AddAddress( 'jai.parekh@hdbfs.com' );
	//$mis_email->AddAddress( 'marketing.support@hdbfs.com' );
	$mis_email->AddAddress( 'venkata.swamy@hdbfs.com' );
	$mis_email->AddAddress( 'sanjay.belsare@hdbfs.com' );
		
		if (!$mis_email->send()) {
			echo "Mailer Error: " . $mis_email->ErrorInfo;
		} else {
			echo "MIS Message sent! <br><br>";
			echo $table_html;
		}

}
}
?>