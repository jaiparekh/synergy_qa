<?php include('body_header.php'); 
session_start();
$transaction_id = $_SESSION['transaction_id'];
?>	
<h3>Terms &amp; Conditions</h3>
<ol>
  <li>I consent to receive information, service, etc. for marketing purpose through Telephone/Mobile/SMS/Email by HDBFS / its agents.</li>
  <li>I/We declare that we are  citizen of India and all the particulars and information given in the application  form are true, correct and complete and no material information has been  withheld or suppressed.</li>
  <li>I/We shall advise HDB financial  Services Limited (hereinafter referred to as &quot;HDBFS&quot;) in writing of  any change in my/our residential or employment/business address.</li>
  <li>I/We confirm that the funds  shall be used for the stated purpose and will not be used for speculative or  anti-social purpose.</li>
  <li>I/We authorize HDBFS to conduct  such credit checks as it considers necessary in its sole discretion, to make  any enquiries with other finance companies/registered credit bureau/other  institutions regarding</li>
  <li>my application and to release  such or any other information in its records for the purpose of credit  appraisal/sharing for any other purpose. HDBFS reserves the right to retain  photographs and documents submitted with this application and will not return  the same to the applicant.</li>
  <li>I/We understand that the  sanction of this loan is at the sole discretion of HDBFS and upon my/our  executing necessary security(ies) and other formalities as required by HDBFS.  I/We further agree that my/our loan shall be governed by the rules of HDBFS  which may be in force from time to time.</li>
  <li>Payment: No cash/bearer cheque  has been collected from me upfront towards processing of the loan application.  All payments made to HDBFS shall be in the name of &quot;HDB Financial Services  Limited&quot; only. I/We will not be entitled to avail of any  benefits/discounts/Free gifts or any other benefit, which is not documented in  the loan agreement.</li>
  <li>I/We confirm that I/We have no  solvency proceedings against me/us nor have I/We ever been adjudicated  insolvent.</li>
  <li>I/We confirm that I/We am/are  not related to any of the Directors/Senior Officers of HDBFS.</li>
</ol>
<p></p>
<?php include('footer.php'); ?>